<?php

/**
 * @file
 * Handles the layout of the quizfileupload answering form.
 *
 * Variables available:
 * - $form;
 */
print drupal_render($form);

?>
