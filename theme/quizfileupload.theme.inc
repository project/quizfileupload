<?php

/**
 * @file
 * Theme functions for the quizfileupload question type.
 */

/**
 * Theme the quizfileupload answer.
 *
 * @param array $variables
 *
 * @return string
 *   An HTML string.
 */
function theme_quizfileupload_user_answer($variables) {
  $answer = $variables['answer'];
  $correct = $variables['correct'];
  $header = array(t('Correct Answer'), t('User Answer'));
  $row = array(array($correct, $answer));
  return theme('table', array('header' => $header, 'rows' => $row));
}

/**
 * Theme the quizfileupload response form.
 *
 * @param array $variables
 *
 * @return string
 *   An HTML string.
 */
function theme_quizfileupload_response_form($variables) {
  $form = $variables['form'];
  return drupal_render_children($form);
}
